import React, { Component } from 'react';
import { KeyboardAvoidingView, View, TextInput, StyleSheet } from 'react-native';

class KeyboardAvoidingViewPage extends Component {
    render() {
    return (
        <View>
          <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            <TextInput 
             placeholder="blahblah"/>
          </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {

    }
})

export default KeyboardAvoidingViewPage;
