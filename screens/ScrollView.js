import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollViewS } from 'react-native';

export default class ScrollView extends Component {
   state = {
      names: [
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},
         {'Budoy'},

         
      ]
   }
   render() {
      return (
         <View>
            <ScrollView>
               {
                  this.state.names.map((item, index) => (
                     <View key = {item.id} style = {styles.item}>
                        <Text>{item.name}</Text>
                     </View>
                  ))
               }
            </ScrollView>
         </View>
      )
   }
}

const styles = StyleSheet.create ({
   item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#d2f7f1'
   }
})